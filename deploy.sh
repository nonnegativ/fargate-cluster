#!/bin/bash

set +x
set +e

sam build --use-container --profile=nonnegativ
sam deploy --profile=nonnegativ --stack-name FargateStack --capabilities CAPABILITY_NAMED_IAM $@

# sam delete --region eu-central-1 --stack-name FargateStack

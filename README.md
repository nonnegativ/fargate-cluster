# Fargate cluster SAM template

This SAM template creates VPC, creates an ECS cluster, deploys 2 Fargate-managed
containers behind an internal load balancer, and exposes it through an API Gateway URL.

Note that an alternative is to change "internal" to "internet-facing" and then one could
route traffic directly to the load balancer from Route 53, instead of going through API
Gateway.

This took far too much effort to get working than it should have, so I will make the
template public for others to use as a reference.

Figuring out all the details here was a collaboration between myself and ChatGPT, who
knows a lot about AWS but likes to withold information and lie to you.

## Usage

### Preparation

First, create an ECR repository using the AWS console, compile the `Dockerfile` here, like
so, tag it, and push it:

    docker build -t my-ecr-repository .
    docker tag my-ecr-repository:latest NNN.dkr.ecr.eu-central-1.amazonaws.com/my-ecr-repository:latest
    docker push NNN.dkr.ecr.eu-central-1.amazonaws.com/my-ecr-repository:latest

Of course, replace NNN with your AWS account ID, and replace `eu-central-1` with whatever
region is appropriate for you.

If you don't have SAM available, install it, creating a virtual environment if necessary:

    python3 -m venv venv
    . venv/bin/activate
    pip install aws-sam-cli

### Deployment

You should to provide your repository as a parameter override:

    ./deploy.sh --parameter-overrides Repository=NNN.dkr.ecr.eu-central-1.amazonaws.com/some-other-ecr-repo

Alternatively you can add your own `Default` value to the `Parameters` block at the end.
I was unable to provide it here because apparently you can't subsitute `${AWS::AccountId}`
and `${AWS::Region}` in a `Default` field.

### Testing

If deployment is successful, you should have an API gateway URL available:


    Outputs                                                                                         
    -------------------------------------------------------------------------------------------------
    Key                 ApiEndpoint                                                                 
    Description         Endpoint URL to use for accessing the application running on Fargate.       
    Value               https://zbgjsvz9ad.execute-api.eu-central-1.amazonaws.com/ 

You can then use `curl` to test it:

    $ curl https://zbgjsvz9ad.execute-api.eu-central-1.amazonaws.com/
    {"message":"Hello, World!"}

Good luck!

-- Steve
